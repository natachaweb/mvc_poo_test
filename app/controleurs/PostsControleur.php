<?php
/*
./app/controleurs/PostsControleur.php
 */

namespace App\Controleurs;

class PostsControleur extends \Noyau\Classes\ControleurGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_gestionnaire = new \App\Modeles\PostsGestionnaire();
  }

  public function indexByCategorieAction(int $categorieId){
    $posts = $this->_gestionnaire->findAllByCategorie($categorieId);
    include '../app/vues/posts/liste.php';
  }


}
